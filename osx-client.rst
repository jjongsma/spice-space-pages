OSX Client
###############################################################################

:slug: osx-client
:modified: 2016-01-06 10:28

.. _bundle: http://people.freedesktop.org/~teuf/spice-gtk-osx/dmg/0.5.7/RemoteViewer-0.5.7-1.dmg
.. _jhbuild: https://wiki.gnome.org/action/show/Projects/Jhbuild
.. _gtk-osx: http://www.gtk.org/download/macos.php
.. _spice-osx moduleset:
.. _this repository: http://cgit.freedesktop.org/spice/spice-jhbuild/log/?h=osx
.. _gtk-osx project:
.. _OSX bundle: https://wiki.gnome.org/Projects/GTK%2B/OSX/Building
.. _spice-osx-build-setup.sh script: http://cgit.freedesktop.org/spice/spice-jhbuild/plain/spice-osx-build-setup.sh?h=osx
.. _gtk-mac-bundler: https://wiki.gnome.org/Projects/GTK%2B/OSX/Bundling

Status
++++++

A bundle_ is now available.

- Works with either gtk2 or gtk3
- Uses GStreamer for audio (output and input?)
- No USB support
- No smartcard support

How to build
++++++++++++

The MacOSX client uses the jhbuild_ moduleset provided by the `gtk-osx`_ project. `This repository`_ contains an additional moduleset to build the needed SPICE modules. The OSX client is based on remote-viewer (as the Windows client).

Prerequisite
^^^^^^^^^^^^

Before using it, you need to install XCode. I'm using XCode 4.3.3 for Lion.
After installing it, I had to run the command below to setup everything
as expected:

.. code-block:: sh

  sudo xcode-select --switch /Applications/Xcode.app/ContentsDeveloper

If you want to build spice-gtk from git (the default moduleset is building it
from tarballs), you also need the Perl module Text::CSV. To install it you can run:

.. code-block:: sh

  sudo cpan Text::CSV

Setup
^^^^^

Once these prerequisite are addressed, you can download the `spice-osx-build-setup.sh script`_ and run it:

.. code-block:: sh

  sh spice-osx-build-setup.sh

It will download the gtk-osx jhbuild moduleset using the gtk-osx-build-setup.sh script from the `gtk-osx project`_. This in turn installs jhbuild in
~/Source and creates ~/.local/bin/jhbuild. It will also install ~/.jhbuildrc
and ~/.jhbuildrc-custom and will copy the current gtk-osx modules into
~/Source/jhbuild/modulesets. 

After the script runs, ~/Source/spice-jhbuild will contain a jhbuildrc file.
You can copy it to ~/.jhbuildrc-custom and jhbuild (as setup by the ~/.jhbuildrc
file created by gtk-osx) will pick it automatically. Alternatively, you can copy
it to ~/.jhbuildrc-spice and set the JHB environment variable to 'spice' (or
use any other prefix of your liking). You can edit this jhbuildrc file if you
want to configure the location where source will be downloaded/expanded, and
the location where the built binaries/libraries will be installed.

Building
^^^^^^^^

Make sure ~/.local/bin is in your PATH, this is where the jhbuild binary is
located. You can then run

.. code-block:: sh

  jhbuild bootstrap
  jhbuild build

This will download and build virt-viewer and all its dependencies. Once this is
done, you can start remote-viewer with

.. code-block:: sh

  jhbuild run remote-viewer


If a weird build error happens while building freetype, exit jhbuild, run

.. code-block:: sh

  jhbuild buildone -cfa freetype

and then restart jhbuild with

.. code-block:: sh

  jhbuild build

Bundling
++++++++
The SPICE jhbuild moduleset also comes with the needed files to generate an `OSX bundle`_ for easy deployment. This uses gtk-mac-bundler from the `gtk-osx project`_. 

After building virt-viewer or virt-viewer-gtk3, make sure you have installed `gtk-mac-bundler`_. You can then use the bundle/remote-viewer.bundle and bundle/remote-viewer-gtk3.bundle files that come with the `spice-osx moduleset`_ to generate a bundle. If you used the spice-osx-build-setup.sh, the bundle file name will be ~/Source/spice-jhbuild/bundle/remote-viewer.bundle. Once it is installed, you can call

.. code-block:: sh

  jhbuild run gtk-mac-bundler ~/Source/spice-jhbuild/bundle/remote-viewer.bundle
  
This will create a RemoteViewer.app bundle on your desktop containing everything that is needed to run the remote-viewer binary you just built on any system.

Finally, you then generate a dmg file from this bundle from the commandline using

.. code-block:: sh

  hdiutil create ~/Desktop/dist/RemoteViewer.dmg -srcfolder ~/Desktop/dist/ -ov
