Support
###############################################################################

:slug: support
:modified: 2015-10-20 10:20

.. _Spice User Manual: spice-user-manual.html
.. _spice for newbies: {filename}/static/docs/spice_for_newbies.pdf
.. _Spice list archive: http://lists.freedesktop.org/archives/spice-devel/
.. _opened bugs: https://bugs.freedesktop.org/describecomponents.cgi?product=Spice
.. _bug-tracker: https://bugs.freedesktop.org/enter_bug.cgi?product=Spice
.. _feature-tracker: https://bugs.freedesktop.org/enter_bug.cgi?product=Spice&component=RFE
.. _Features: features.html
.. _search: https://bugs.freedesktop.org/query.cgi?product=Spice
.. _opened RFEs: https://bugs.freedesktop.org/buglist.cgi?component=RFE&list_id=560478&product=Spice&resolution=---

Using Spice for the first time?
+++++++++++++++++++++++++++++++

`Spice user manual`_ provides all the information on how to get, build, install and use Spice. You can also read `Spice for newbies`_ in case you want to have a better understanding about Spice architecture and features.

Need help using Spice?
++++++++++++++++++++++

Many questions can be answered in the `Spice user manual`_ and the `Spice list archive`_. If you can't find the answer there, post your question to spice-devel@lists.freedesktop.org. Be sure to detail your issue so that we can offer the best response.

Have some feedback to share?
++++++++++++++++++++++++++++
Spice project would like to have your feedback - either negative or positive. Please post your feedback to spice-devel@lists.freedesktop.org.

Found a bug? Want to help and report?
+++++++++++++++++++++++++++++++++++++
#. Look at `opened bugs`_ to see whether it is a known bug.

   - There is no need to open a new bug in case it is a known bug, but optionally you can add your comment to the existing bug report or add yourself to the CC list.
   - In case you are not using the latest version of Spice, upgrade and see if it was resolved.

#. Open a new bug report in Spice `bug-tracker`_ that follow these guidelines:

   - Write clear and detailed description of the bug.
   - Provide instructions on how to reproduce the bug.
   - Specify the relevant components and their versions.
   - Attach relevant logs.

     - Spice server send its log messages to stdout, so you will need to redirect QEMU stdout to a file in order to have a server log file.
     - Linux Spice client log is /tmp/spicec.log, Windows Spice client log is in %temp%\spicec.log.

   - Provide the necessary information:

     - Crash - provide the crash message.
     - Client bug - specify the OS type and version.
     - Wrong rendering - attach a screen shot. (You will have to take client screen shot so make sure that the cursor is not on Spice window area while taking the screen shot).

Your help for tracking down the bug is very important, so please try to be as cooperative as possible and help us resolve the bug. 

Like to have a new feature?
+++++++++++++++++++++++++++
First look at Spice `opened RFEs`_ or `search`_ to see whether it was already requested. In addition, refer to the Features_ page. If it was not requested you can submit your request in Spice `feature-tracker`_ using a clear and detailed description.
