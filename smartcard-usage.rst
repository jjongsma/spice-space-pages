Smartcard usage
###############################################################################

:slug: smartcard-usage
:modified: 2016-01-14 11:28

.. _esc for windows: http://directory.fedoraproject.org/docs/389ds/FAQ/esc-guide.html#windows

This page is meant to get people started with smartcard support in Spice.

QEMU
++++

Compilation
^^^^^^^^^^^

First make sure your spice-server is compiled with smartcard support (--enable-smart must have been passed to autogen.sh/configure).
QEMU must also be compiled with smartcard support. All you have to do is to make sure you passed --enable-smartcard --enable-smartcard-nss to QEMU's configure.

Running QEMU and exporting a smartcard spice channel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can start a VM with smartcard support using

.. code-block:: sh

  qemu-system-x86_64 #allyouroptions# 
       -spice addr=127.0.0.1,port=5930,disable-ticketing 
       -chardev spicevmc,name=smartcard,id=ccid  
       -device usb-ccid -device ccid-card-passthru,chardev=ccid -usb

Breakdown of the parameters
---------------------------

This just explains the above parameters for those interested:

- '''-usb''' enable the usb bus (this is a usb 1.1 host, I think uhci)
- '''-device usb-ccid''' create a single usb-ccid device on the usb bus
- '''-chardev spicevmc,name=smartcard,id=ccid''' create a chardev of type spicevmc with subtype smartcard (just a tag) and id ccid (the id is referenced by the next qemu device)
- '''-device ccid-card-passthru,chardev=ccid''' create a ccid-card-passthru device on the usb-ccid bus (there is only one usb-ccid device in this example so no need to id it) using the created chardev to communicate to the world.

Using a physical smartcard
++++++++++++++++++++++++++

Preliminary Setup for the physical smartcard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install pcscd on the client and make sure you can use your smartcard with nss

.. code-block:: sh

  mkdir -p /etc/pki/nssdb
  certutil -N -d /etc/pki/nssdb

Add your library to the database to access your smartcard (mine is aet)

.. code-block:: sh

  modutil -add "aet" -dbdir /etc/pki/nssdb -libfile /usr/lib/libaetpkss.so

or coolkey

.. code-block:: sh

   modutil -add "coolkey" -dbdir /etc/pki/nssdb -libfile /usr/lib/pkcs11/libcoolkeypk11.so

Make sure you can show the certificates (mine asks for a pin)

.. code-block:: sh

  certutil -L -d sql:/etc/pki/nssdb -h all

Spice Client with the the physical smartcard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now we can start the spice client with the smartcard option (pay attention to the blinking led if any on the smartcard reader)

.. code-block:: sh

  remote-viewer --spice-smartcard spice://127.0.0.1:5930

Login to the VM and make sure a smartcard reader appears in the VM

.. code-block:: sh

  lsusb|grep -i gem

If you have the device install pcscd and coolkey

.. code-block:: sh

  apt-get install pcscd coolkey
  mkdir -p /etc/pki/nssdb
  certutil -N -d /etc/pki/nssdb

The virtual smartcard is based on the CAC standard and coolkey uses this standard so use this to access your card

.. code-block:: sh

  modutil -add "coolkey" -dbdir /etc/pki/nssdb -libfile /usr/lib/pkcs11/libcoolkeypk11.so
  certutil -L -d sql:/etc/pki/nssdb -h all

If right your certificates would show up on the vm

Using a software smartcard
++++++++++++++++++++++++++

Preliminary Setup
^^^^^^^^^^^^^^^^^

To emulate a software smartcard, we need to generate 3 certificates which will be used for the "fake" smartcard.
This can be done using

.. code-block:: sh

  mkdir ~/.netscape
  certutil -N (use an empty password)
  certutil -x -t "CT,CT,CT" -S -s "CN=cert1" -n cert1
  certutil -x -t "CT,CT,CT" -S -s "CN=cert2" -n cert2
  certutil -x -t "CT,CT,CT" -S -s "CN=cert3" -n cert3

After doing this, cert1, cert2 and cert3 should be listed when typing

.. code-block:: sh

  certutil -L

Spice Client
^^^^^^^^^^^^

Now you can start the client. You need to give it the name of the certificates to use for the software smartcard:

.. code-block:: sh

  remote-viewer spice://localhost:5924 --spice-smartcard --spice-smartcard-db ~/.netscape/ --spice-smartcard-certificates cert1,cert2,cert3

Login to the VM and make sure a smartcard reader appears in the VM

.. code-block:: sh

  lsusb|grep -i gem

The software smartcard is initially inserted in the virtual smartcard reader in the VM, this can be checked with pcsc_scan. Card removal can be simulated with shit+F9, and card (re)insertion with shit+F8. 

Testing
^^^^^^^

In the VM, make sure you have pcscd and its tool installed, and that it's running. On Debian/Ubuntu the package to install is pcscd, on 
Fedora it's pcsc-tools, and you need to start the daemon with service pcscd start.

The smartcard reader state can be queried with pcsc_scan, and you can check which certificates are available on the smartcard by running pkcs11_listcerts. When using a software smartcard, it's  You can also install the "esc" graphical tool to manage your smartcards.

Firefox setup to see certificates from smart card
+++++++++++++++++++++++++++++++++++++++++++++++++

You need to add the libcoolkeypk11.so provider, see:
- http://www.gooze.eu/howto/iceweasel-firefox-smartcard-and-token-howto/configuring-the-navigator-to-use-smart-cards

Linux Esc
+++++++++

This is a gecko based smartcard manager.

For fedora:

.. code-block:: sh

  dnf install esc

Windows Esc
+++++++++++
For windows: `esc for Windows`_
- The download link is broken in that page, use: http://www.nabber.org/projects/coolkey/

Debugging
+++++++++

According to what can go wrong

Are the certificates visible in the client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If this is a software backed card, and you followed the above instructions, then certutil -L should give:

.. code-block:: sh

  $ certutil -L
  
  Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI
  
  cert2                                                        CTu,Cu,Cu
  cert3                                                        CTu,Cu,Cu
  cert1                                                        CTu,Cu,Cu

If this is a real smartcard, likewise.

Is the usb device visible inside the guest
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ccid reader should show up doing lsusb or from device manager.

.. code-block:: sh

  $ lsusb | grep Gemplus
  Bus 001 Device 002: ID 08e6:4433 Gemplus GemPC433-Swap

Does spice client see the reader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

  $ cat ~/.spicec/spicec.log  | grep Smart
  1322144481 INFO [28131:28139] SmartCardChannel::cac_card_events_thread_main: VEVENT_READER_INSERT
  1322144481 INFO [28131:28139] SmartCardChannel::cac_card_events_thread_main: VEVENT_CARD_INSERT
  1322144481 INFO [28131:28131] SmartCardChannel::add_unallocated_reader: adding unallocated reader 0x1ed0570
  1322144481 INFO [28131:28131] SmartCardChannel::add_reader: adding 0x1ed0570->0

Does the guest have the coolkey provider?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: sh

  $ modutil -list -dbdir /etc/pki/nssdb/
 
  Listing of PKCS #11 Modules
  -----------------------------------------------------------
   1. NSS Internal PKCS #11 Module
      slots: 2 slots attached
      status: loaded
 
      slot: NSS Internal Cryptographic Services
      token: NSS Generic Crypto Services
 
      slot: NSS User Private Key and Certificate Services
      token: NSS Certificate DB
 
   2. CoolKey PKCS #11 Module
      library name: libcoolkeypk11.so
      slots: 1 slot attached
      status: loaded
 
      slot: Gemalto GemPC433 SL [CCID Interface] (1) 00 00
      token: cert1
  -----------------------------------------------------------

Does the coolkey module load?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As an alternative to the above, you could try to add the module and see that it actually loads using something like this (in the guest of course):

.. code-block:: sh

  # NSS_DEBUG_PKCS11_MODULE="coolkey" modutil -add coolkey -dbdir /etc/pki/nssdb/ -libfile /usr/lib/pkcs11/libcoolkeypk11.so

  WARNING: Performing this operation while the browser is running could cause
  corruption of your security databases. If the browser is currently running,
  you should exit browser before continuing this operation. Type 
  'q <enter>' to abort, or <enter> to continue: 

  ERROR: Failed to add module "coolkey". Probable cause : "Failure to load dynamic library".
  Function                     # Calls         Time         Avg.     % Time
  
  C_Initialize                       1       1000us    1000.00us      0.14%
  C_Finalize                         1       1000us    1000.00us      0.14%
  C_GetInfo                          1          0 z       0.00us      0.00%
  C_GetSlotList                      2        724ms  362000.00us     99.59%
  C_GetSlotInfo                      2          0 z       0.00us      0.00%
  C_GetTokenInfo                     1          0 z       0.00us      0.00%
  C_GetMechanismList                 2       1000us     500.00us      0.14%
  C_OpenSession                      1          0 z       0.00us      0.00%
  C_CloseAllSessions                 1          0 z       0.00us      0.00%
  C_GetSessionInfo                   1          0 z       0.00us      0.00%
  C_FindObjectsInit                  1          0 z       0.00us      0.00%
  C_FindObjects                      1          0 z       0.00us      0.00%
  C_FindObjectsFinal                 1          0 z       0.00us      0.00%

                     Totals         16        727ms


  Maximum number of concurrent open sessions: 1


Does the guest see the certificates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pcsc_scan is not available in all installations (RHEL doesn't have it), and also only provides ATR + card inserted/removed status. Use pkcs11_listcerts to get the certs: (for the emulated card there is no pin, use <enter>)

.. code-block:: sh

  # pkcs11_listcerts 
  PIN for token: 
  Found '3' certificate(s)
  Certificate #1:
  - Subject:   CN=cert1
  - Issuer:    CN=cert1
  - Algorithm: PKCS #1 RSA Encryption
  verify_certificate() failed: 
  Certificate #2:
  - Subject:   CN=cert2
  - Issuer:    CN=cert2
  - Algorithm: PKCS #1 RSA Encryption
  verify_certificate() failed: 
  Certificate #3:
  - Subject:   CN=cert3
  - Issuer:    CN=cert3
  - Algorithm: PKCS #1 RSA Encryption
  verify_certificate() failed: 

Verification failure is normal. The certificates are self signed.

How to get log of smartcard messages sent on wire?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Actual messages passed by libcacard library (part of qemu sources). To get them, the easiest way is to run client with libcacard debug output enabled on the client:

.. code-block:: sh

  G_MESSAGES_DEBUG=libcacard LIBCACARD_DEBUG=1 remote-viewer --spice-smartcard [URI or .vv file]
