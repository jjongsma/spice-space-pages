Documentation
###############################################################################

:slug: documentation
:submenu: documentation
:modified: 2015-10-13 09:00

.. _spice for newbies: {filename}/static/docs/spice_for_newbies.pdf
.. _spice protocol: {filename}/static/docs/spice_protocol.pdf
.. _ODP version: {filename}/static/docs/spice_redhat_summit_2009.odp
.. _PDF version: {filename}/static/docs/spice_redhat_summit_2009.pdf
.. _spice style: spice-project-coding-style-and-coding-conventions.html
.. _UsbDk Presentation: {filename}/static/docs/UsbDk_at_a_Glance.pdf
.. _UsbDk Manual: {filename}/static/docs/UsbDk_Software_Development_Manual.pdf
.. _Reference Manual: spice-user-manual.html
.. _spice-gtk API: spice-gtk.html
.. _agent protocol: agent-protocol.html

Spice `Reference Manual`_
+++++++++++++++++++++++++

Describes the various features provided by SPICE, and how to make use of
them when using QEMU/libvirt/virt-manager.

`spice-gtk API`_ documentation
++++++++++++++++++++++++++++++

Describes spice-gtk C API.

`Spice For Newbies`_ document
+++++++++++++++++++++++++++++
Contains basic information about Spice's architecture, components, and features.

`SPICE Protocol`_ documentation
+++++++++++++++++++++++++++++++

Outlines the complete definition of the SPICE remote computing protocol and SPICE client <-> `agent protocol`_

`Spice style`_ documentation
++++++++++++++++++++++++++++

Defines Spice project's coding style and coding conventions.

Red Hat Summit 2009
+++++++++++++++++++

Spice session presentation at Red Hat Summit 2009

Download `ODP version`_ or `PDF version`_

UsbDk
+++++

Download `UsbDk Presentation`_ and `UsbDk Manual`_
