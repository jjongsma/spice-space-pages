FAQ
###############################################################################

:slug: faq
:modified: 2015-10-12 12:20

.. _Download: download.html
.. _Documentation: documentation.html
.. _Developers: developers.html
.. _Spice User Manual: spice-user-manual.html
.. _Spice for Newbies: {filename}/static/docs/spice_for_newbies.pdf
.. _Support: support.html

Why Spice?
++++++++++

Adequate user experience and the lack of a good solution for virtual machine
remote access were what sparked the Spice project. To ensure that Spice is a
success, the following project goals were set:

#. To deliver a high-quality user experience, similar to local machine, in LAN
   environments
#. To maintain low CPU consumption in order to have high VM density on the host
#. To provide high-quality video streaming

Why choose Spice?
+++++++++++++++++

- Spice offers greater user experience and increased usability and reliability
- Spice has a lot more space to evolve
- Spice is open source
- Spice architecture is cross-platform, allowing for greater interoperability

Under what license is Spice distributed?
++++++++++++++++++++++++++++++++++++++++

Most of Spice sources are distributed under GPL v2. For some exceptions the
project uses LGPL or BSD style licenses. Documents are distributed under
Creative Commons Attribution-Share Alike 3.0 license.

How does Spice work?
++++++++++++++++++++

Download `Spice for Newbies`_ for information on Spice basic architecture,
components, and features.

How do I get Spice?
+++++++++++++++++++

Refer to our Download_ page or the `Spice User Manual`_.

How do I use Spice?
+++++++++++++++++++

Consult `Spice User Manual`_ for detailed information.

How can I contribute to Spice?
++++++++++++++++++++++++++++++

Your help and feedback are welcomed. If you are a Spice user, refer to the
Documentation_ page. For developers, refer to the Developers_ page.

Spice has poor video performance
++++++++++++++++++++++++++++++++

- Verify QXL driver is properly installed and running
- Try adjusting QXL video memory to 64Mb (see also discussion in https://bugs.freedesktop.org/show_bug.cgi?id=36186)
- Flash video playback produce worst performance when there is lot of overlaying 2d elements (statusbar, seekbar, ads, buttons etc...)

Which architecture can Spice run on ?
+++++++++++++++++++++++++++++++++++++

- the client can run on x86, x86_64 and arm architectures.
- the server does only work on little-endian architectures, and has mainly been tested on x86-64.

Where can I find support ?
++++++++++++++++++++++++++

Have a look at the Support_ page. It provides links to manuals, mailing-list, and bug trackers.
