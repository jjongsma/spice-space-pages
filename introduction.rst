Introduction
###############################################################################

:slug: introduction
:modified: 2015-10-13 10:30

The Spice project aims to provide a complete open source solution for
interaction with virtualized desktop devices.The Spice project deals with both
the virtualized devices and the front-end. Interaction between front-end and
back-end is done using VD-Interfaces. The VD-Interfaces (VDI) enable both ends
of the solution to be easily utilized by a third-party component.

The following diagram illustrates VD-Interfaces:

.. image:: {filename}/static/images/vdi_schem.png
    :alt: VDI scheme

Currently, the project main focus is to provide high-quality remote access to
QEMU virtual machine. Seeking to help break down the barriers to virtualization
adoption by overcoming traditional desktop virtualization challenges,
emphasizing user experience. For this purpose, Red Hat introduced the SPICE
remote computing protocol that is used for Spice client-server communication.
Other components developed include QXL display device and driver, etc.

The following diagram illustrates the current Spice solution on top of QEMU.

.. image:: {filename}/static/images/spice_schem.png
    :alt: SPICE scheme

The Spice project plans to provide additional solutions, including:

- Remote access for a physical machine
- VM front-end for local users (i.e., render on and share devices of the
  same physical machine)

Like to know whats going on? Join spice-devel@lists.freedesktop.org to
get information about all things related to the Spice project. For
other mailing lists and irc channel see the contact page
